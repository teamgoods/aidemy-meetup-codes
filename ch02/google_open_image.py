import sqlite3
import numpy as np
import glob
import matplotlib.pyplot as plt
from PIL import Image

#conn = sqlite3.connect("google_open_images.db")
#cur = conn.cursor()
#
#with open("over1000labelname.txt",'r') as f:
#    label_name = f.readline().strip()
#    while label_name:
#        
#        cur.execute("""select description from class where LabelName = '{}'""".format(label_name))
#        description = cur.fetchall()[0]
#
#        cur.execute("""select ImageID from train_image_labels where LabelName = '{}'""".format(label_name))
#        image_cnt = len(cur.fetchall())
#        
#        print(label_name,'\t',description,'\t',image_cnt)
#        
#        label_name = f.readline().strip()
#
#conn.close()


def get_data(select_labels):
    conn = sqlite3.connect("google_open_images.db")
    cur = conn.cursor()
    arr = np.empty((0,100,100,3), int)
    for e,select_label in enumerate(select_labels):
        print("fetching ",select_label)
        cur.execute("""select LabelName from class where description = '{}'""".format(select_label))
        labelname = cur.fetchall()[0][0]
        cur.execute("""select ImageID from train_image_labels where LabelName = '{}' limit 2000""".format(labelname))
        for imageid in cur.fetchall():
            imageid = imageid[0]
            try:
                img = Image.open("./google_open_images/over1000images/{}.jpg".format(imageid))
            except:
                continue

            img = img.resize((100,100))
            imgarray = np.array(img,"f")
            if len(imgarray.shape) < 3:
                continue
            imgarray = imgarray.reshape(-1,100,100,3)
            arr = np.append(arr, imgarray, axis=0)
            if arr.shape[0] > 1000*(e+1):
                break
    conn.close()
    return arr
