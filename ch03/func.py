

from scipy import arange, hamming, sin, pi
from scipy.fftpack import fft, ifft
import numpy as np
import librosa
import matplotlib.pyplot as plt
import glob

def fft_plot(data,fs, n_mfcc=20, n_fft=None):
    
    # fs = Sampling rate
    L = len(data) # Signal length（サンプル数）

    if n_fft == None:
        n_fft = L

    data_length = L /fs
    delta_freq = 1/data_length
    
    # 窓関数
    win = hamming(L)

    # フーリエ変換
    spectrum = fft(data * win) # 窓関数あり
    half_spectrum = abs(spectrum[: int(L / 2) + 1])
    
    # mfccs translation
    mfccs = mfcc(data, sr=fs, n_mfcc=n_mfcc, n_fft=n_fft)
    
    # 図を表示
    fig = plt.figure(figsize=(18,12))

    fig.add_subplot(311)
    plt.plot(data)
    plt.xlim([0, L])
    plt.grid()
    plt.title("1. Input signal", fontsize = 15)

    fig.add_subplot(312)
    plt.plot(half_spectrum)
    plt.xticks([i for i in range(len(half_spectrum))][::100],[i*delta_freq for i in range(len(half_spectrum))][::100])
    plt.grid()
    plt.title("2. Spectrum (with window)", fontsize = 15)

    fig.add_subplot(313)
    plt.plot(mfccs[1:])
    plt.grid()
    plt.title("3. MFCC (over 1)", fontsize = 15)
    
    plt.show()

    return (np.arange(len(spectrum))*delta_freq, spectrum)


from librosa.core import stft, spectrum
from librosa.filters import mel, dct

def mfcc(y, sr=22050, n_mfcc=20, n_fft=2048, center=False, power=2.):

    abs_spectrum = np.abs(spectrum.stft(y, n_fft=n_fft, center=False))**power

    mel_basis = mel(sr, n_fft, n_mels=512)
    melspec = np.dot(mel_basis, abs_spectrum)

    S =spectrum.power_to_db(melspec)

    return np.dot(dct(n_mfcc, S.shape[0]), S)

import sqlite3
import collections

def get_gender_speaker_dict():

    print("Connecting Database...")
    conn = sqlite3.connect("./train-clean-100.db")
    cur = conn.cursor()
    cur.execute('''select distinct readerID, gender from data''' )
    obj = cur.fetchall()
    conn.close()

    print(collections.Counter([j for i,j in obj]))
    gender_dict = {k:v for k,v in obj}
    return gender_dict

def get_id_speaker_dict():

    print("Connecting Database...")
    conn = sqlite3.connect("./train-clean-100.db")
    cur = conn.cursor()
    cur.execute('''select readerID from data''' )
    obj = cur.fetchall()
    conn.close()

    print(collections.Counter(obj))
    id_speaker_dict = {k:v for k,v in enumerate(list(set(obj)))}
    return id_speaker_dict


def load_wav(file_path):
    from scipy.io.wavfile import read

    wavfile = file_path

    fs, data = read(wavfile)

#     print("Sampling rate :", fs)
#     print("Sample num :",len(data))
    
    return fs,data


def write_wav(file_path, fs, data):
    from scipy.io.wavfile import write
    try:
        write(file_path,fs, data)
        return 1
    except:
        return 0

def dens_threshold(data, th, win=100):
    # 閾値
    L = len(data)
    abs_sum = []
    for i in range(int(L/100)):
        d = data[i*100:(i+1)*100]
        abs_sum.append(np.sum(np.array([np.abs(x) for x in d]))//100.)

    pos = 0; neg = 0
    for i in abs_sum:
        if i >= 800.:
            pos += 1
        else:
            neg += 1
#     print("pos:",pos,"neg:",neg,"/",int(L/100))
    if pos/int(L/100) > 0.5:
        return 1
    else:
        return 0
    
    
import librosa

def add_delta_mfcc(x_train, x_test):
    shape = x_train.shape
    X_train = x_train.reshape(shape[0], shape[1], 1)
    shape = x_test.shape
    X_test = x_test.reshape(shape[0], shape[1], 1)
    
    delta = librosa.feature.delta
    
    Xd_train = np.array([delta(x) for x in X_train])
    Xd_test = np.array([delta(x) for x in X_test])
    
    X_train = np.concatenate((X_train, Xd_train), axis = 1)
    X_test = np.concatenate((X_test, Xd_test), axis = 1)
    
    return (X_train, X_test)
    
def add_delta_delta_mfcc(x_train, x_test):
    shape = x_train.shape
    X_train = x_train.reshape(shape[0], shape[1], 1)
    shape = x_test.shape
    X_test = x_test.reshape(shape[0], shape[1], 1)
    
    delta = librosa.feature.delta
    
    Xd_train = np.array([delta(x) for x in X_train])
    Xd_test = np.array([delta(x) for x in X_test])
    
    Xdd_train = np.array([delta(x) for x in Xd_train])
    Xdd_test = np.array([delta(x) for x in Xd_test])
    
    X_train = np.concatenate((X_train, Xd_train, Xdd_train), axis = 1)
    X_test = np.concatenate((X_test, Xd_test, Xdd_train), axis = 1)
    
    return (X_train, X_test)


def make_speaker_datasets(data_path, frame_length = 4096, stride=None):
    all_data = np.empty((0, frame_length), int)
    all_labels = []
    phoneme_dict = {'a':0, 'i': 1, 'u': 2, 'e': 3, 'o':4}
    
    if stride == None:
        stride = frame_length//2 
    
    name_parser = lambda x: x.split("/")[-1].split("_")[0]
    
    for path in glob.glob(data_path + "/*"):
        
        fs,data = load_wav(path)

        print("Load\t",path)
        print("Length\t",data.shape)

        stride = frame_length//2

        frame_num = int(math.ceil((data.shape[0]-frame_length)/stride))

        if frame_num == 0:
            if len(data) < frame_length: # if data is short, do zero padding.
                data = np.pad(data, (0, frame_length - len(data)), 'constant')
            if not dens_threshold(data[:sample_length], 200): # if data volume is too small, skip.
                continue

            all_data = np.append(all_data, np.array([data[:frame_length]]), axis=0)

            label = name_parser(path)
            all_labels.append(label)

        else:

            for i in range(frame_num):

                if not dens_threshold(data[i*stride:i*stride+frame_length], 200): # if data volume is too small, skip.
                    continue

                all_data = np.append(all_data, np.array([data[i*stride:i*stride+frame_length]]), axis=0)

                label = name_parser(path)                    
                all_labels.append(label)

    print("all_data.shape is ", all_data.shape)
    
    speaker_names = list(set(all_labels))
    speaker_id_dict = {k:v for v,k in enumerate(speaker_names)}
    id_speaker_dict = {v:k for k,v in speaker_id_dict.items()}
    
    all_labels = np.array([speaker_id_dict[l] for l in all_labels])

    return all_data, all_labels, speaker_id_dict, id_speaker_dict





import math


def make_phoneme_datasets(data_path, frame_length = 1200, data_type="aiueo"):
    all_data = np.empty((0, frame_length), int)
    all_labels = []
    phoneme_dict = {'a':0, 'i': 1, 'u': 2, 'e': 3, 'o':4}
    
    
    julius_parse = lambda x: x.split("/")[3].split("_")[6].split(".")[0] 
    aiueo_parse = lambda x: x.split("/")[-1].split("_")[-1].split(".")[0] 
    
    for path in glob.glob(data_path + "/*"):
        
        # juliusデータの場合とaiueoデータの場合
        if data_type == "julius":
            parser = julius_parse
            phone = ["a:", "i:", "u:", "e:", "o:"]
        elif data_type == "aiueo":
            parser = aiueo_parse
            phone =  ["a", "i", "u", "e", "o"]
            
            
        if parser(path) in phone:
            
            fs,data = load_wav(path)
            
            print("Load\t",path)
            print("Length\t",data.shape)
            
            stride = frame_length//2
            
            frame_num = int(math.ceil((data.shape[0]-frame_length)/stride))
            
            
            if frame_num == 0:
                if len(data) < frame_length: # if data is short, do zero padding.
                    data = np.pad(data, (0, frame_length - len(data)), 'constant')
                if not dens_threshold(data[:sample_length], 200): # if data volume is too small, skip.
                    continue
                    
                all_data = np.append(all_data, np.array([data[:frame_length]]), axis=0)

                label = phoneme_dict[parser(path)]
                all_labels.append(label)
            
            else:
                
                for i in range(frame_num):
                    
                    if not dens_threshold(data[i*stride:i*stride+frame_length], 200): # if data volume is too small, skip.
                        continue
                    
                    all_data = np.append(all_data, np.array([data[i*stride:i*stride+frame_length]]), axis=0)

                    label = phoneme_dict[parser(path)]                    
                    all_labels.append(label)
                
    print("all_data.shape is ", all_data.shape)

    return all_data, np.array(all_labels)

