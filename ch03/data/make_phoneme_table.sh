#! /bin/sh

phonemes=()


for filePath in tsuchiya_normal/separated_wav/*; do
  fileName=`basename $filePath .wav`;
  newPhoneme=${fileName##*_};
  count=0;
  for phoneme in ${phonemes[@]}; do
    if [[ $newPhoneme == $phoneme ]]; then
      break;
    fi
    count=$((count + 1));
  done
  if [[ $count == ${#phonemes[*]} ]]; then
    phonemes+=($newPhoneme);
  fi
done

echo ${phonemes[@]}
echo ${phonemes[@]} > phoneme_table.txt
