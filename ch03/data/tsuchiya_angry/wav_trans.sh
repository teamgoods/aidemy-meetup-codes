#!/bin/sh


dirName=`basename \`pwd\``
mkdir wav
for f in default_wav/*.wav; do
  filename=`basename ${f} ".wav"`_for_julius.wav
  sox $f -c 1 -r 16000 -b 16 wav/${filename}
done
