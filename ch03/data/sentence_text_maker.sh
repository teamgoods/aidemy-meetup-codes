#! /bin/sh

if [[ -z $1 ]]; then
  echo 'arg1 is needed for file name'
  exit;
fi

cat $1 | while read line
do
filename=`echo $line | cut -d' ' -f1`
mkdir -p texts
echo ${line} | cut -d' ' -f2 > ./texts/${filename}
done
rm ./texts/sentence_id
