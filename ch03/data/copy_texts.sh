#! /bin/sh

if [[ -z $1 ]]; then
  echo 'arg1 is needed for file name'
  exit;
fi

for file in texts/*; do
  cp $file ${1%/*}/text/${1%/*}_`basename ${file}`_for_julius.txt
done
