#! /bin/sh

for file in ./result/*.lab; do
  count=0;
  cat $file | while read line
  do
    if [[ `echo $line | grep 'sil'` ]]; then
      continue;
    fi
    startTime=`echo $line | cut -d' ' -f1`
    endTime=`echo $line | cut -d' ' -f2`
    duration=`echo "$endTime - $startTime" | bc | sed -e 's/^\./0./g'`;
    wavFile="./separated_wav/`basename $file .lab`_`echo $(printf %02d $count)`_`echo $line | cut -d' ' -f3`.wav"
    sox ./wav/`basename $file .lab`.wav $wavFile trim $startTime $duration;
    count=$((count+1));
  done
done
