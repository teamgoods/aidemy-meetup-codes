#!/bin/sh

path="data/aiueo_sample"

if [ -e /etc/issue ]; then
  for f in ${path}/m4a/*.m4a; do
    avconv -i ${f} ${path}/default_wav/`basename ${f} .m4a`.wav;
    sox ${path}/default_wav/`basename ${f} .m4a`.wav -c 1 -r 16000 -b 16 ${path}/wav/`basename ${f} .m4a`.wav
  done
else
  for f in ${path}/m4a/*.m4a; do
    afconvert -f WAVE -d LEI16 ${f} ${path}/default_wav/`basename ${f} .m4a`.wav;
    sox ${path}/default_wav/`basename ${f} .m4a`.wav -c 1 -r 16000 -b 16 ${path}/wav/`basename ${f} .m4a`.wav
  done
fi
